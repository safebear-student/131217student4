package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class LoginPage {
    WebDriver driver;

    @FindBy(xpath = "//input[contains(@id,'myid')]")
    WebElement username_field;

    @FindBy(xpath = "//input[contains(@id,'mypass')]")
    WebElement password_field;

    @FindAll({
            @FindBy(xpath = " //*[@type='text']")
    })
    List<WebElement> inputfields;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Sign");
    }

    public boolean login(UserPage userPage, String usersname, String password) {
//        this.username_field.sendKeys(usersname);
        inputfields.get(0).sendKeys(usersname);
//        this.password_field.sendKeys(password);
        inputfields.get(1).sendKeys(password);
        this.password_field.submit();
        return userPage.checkCorrectPage();
    }

}
