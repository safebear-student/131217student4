package com.safebear.app;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Tets01_Login.class,
})
public class TestSuite{
}
