package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class Tets01_Login extends BaseTest{
    @Test
    public void testLogin(){
        //Step 1
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 login
        assertTrue(loginPage.login(this.userPage,"testuser","testing"  ));

    }
}
